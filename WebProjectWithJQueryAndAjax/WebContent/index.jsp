<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery, Ajax and Servlet/JSP integration example</title>

<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
	function ajaxCall() {
		$.ajax({
			url : 'UserDetails',
			data : {
				name : $('#userName').val(),
				welcomeMessage : "welcome!"
			},
			success : function(responseText) {
				$('#abcd').text(responseText)
			}
		});
	}
</script>
</head>
<body>

	<form>
		Enter Your Name: <input type="text" id="userName" style="width: 50%"/> <input
			type="button" onclick="ajaxCall();" value="Send Ajax Request">
	</form>
	<br>
	<br>
	<div id="abcd">
	</div>
</body>
</html>