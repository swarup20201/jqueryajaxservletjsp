package com.sample.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserDetails extends HttpServlet{

	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("name").trim();
		String secondndValue = request.getParameter("welcomeMessage");
		if(userName == null ||  userName.equals("")){
			userName = "Guest";
		}
		
		String greetings = "Hello " + userName + " " + secondndValue;
		
		response.setContentType("text/plain");
		response.getWriter().write(greetings);
	}
}
